# Todo App Project Frontend

Frontend App has app ui and called frontend api

## Requirements

[yarn](https://yarnpkg.com/)

[pact](https://github.com/pact-foundation/jest-pact)

## Install, Build And Tests

### Project Setup

```bash
yarn install
```

### Compile development

```bash
yarn dev
```

#### Lints and fixes files

```bash
yarn run lint
yarn run lint-fix
```

### Test Process

#### Run the unit test

```bash
yarn test:unit
```

#### Run the consumer test

```bash
yarn test:contract
```

#### Publish the consumer test

```bash
yarn test:pact:publish
```

## Directory Structure

```bash
.
├── .babelrc
├── client
│   ├── ApiService.js                           - Using for frontend-api call with axios.     
├── components
├── AddTodo.vue                                 - Todo input and button defined here.
│   ├── TodoList.vue                            - This file is calling other components 
│   ├── TodoListItem.vue                        - This compenent has delete and marked item properties          
├── Dockerfile
├── .dockerignore
├── .editorconfig
├── .env.dev
├── .eslintrc.js
├── .gitignore
├── .gitlab-ci.yml
├── jest.config.js                              - Jest configuration file
├── layouts
│   └── default.vue
├── nginx
├── nuxt.config.js                              - Nuxt configuration file
├── package.json
├── pact
│   └── pacts
│       └── frontend-todo-backend-todo.json     - This file has contract log between frontend and frontend project 
├── pages
│   └── index.vue                               - Main view file
├── provision_az_cli_image.sh                   - Bash script that provision the CD stage in the pipeline
├── README.md
├── test
│   ├── contract
│   │   ├── ApiService.pact.interactions.js     - File has request interactions
│   │   └── ApiService.spec.js                  - File has contract testing
│   └── unit
│       ├── AddTodo.spec.js                     - This file has button and input unit tests
│       ├── TodoListItem.spec.js                - This file has general unit test for todolist
│       └── TodoList.spec.js                    - This file has marked , deleted and others unit tests
└── todo-frontend
    ├── Chart.yaml
    ├── .helmignore
    ├── templates
    │   ├── deployment.yml                      - Kubernetes deployment yaml
    │   └── service.yaml                        - Kubernetes service yaml
    └── values.yaml                             - Helm values
```

## Deployment Process

On Gitlab Pipeline

- build
- lint
- unit test
- contract-test
- pact publish
- package
- deploy

## Parameters

The following tables lists the configurable parameters of the Frontend chart and their default values.

|Parameter | Description | Default |
| ------ | ------ | ------ |
| app.name | Deployment name | todo-frontend |
| app.group | Frontend service metadata label | todo-frontend |
| app.replicaCount | Number of Frontend replicas | 1 |
| app.image.name | Frontend image name | "registry.gitlab.com/demo-todo-list/frontend" |
| app.image.tag | Frontend image tag | develop |
| app.image.pullPolicy | Frontend image pull policy | Always |
| app.container.port | Frontend pod port | 3000 |
| app.container.env | List of additional Environment Variables as key/value dictionary | [] |
| app.service.type | Frontend service type | LoadBalancer |
| app.service.port | Frontend service port | 3000 |

### To deploy using helm manually

```bash
helm registry login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
helm chart pull $CI_REGISTRY/$CI_PROJECT_PATH:helm-$CI_BUILD_REF_NAME
helm chart export $CI_REGISTRY/$CI_PROJECT_PATH:helm-$CI_BUILD_REF_NAME -d .
```

## Production URL

http://20.76.2.172:3000/
