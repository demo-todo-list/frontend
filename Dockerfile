# FROM node:lts-alpine as build-stage
FROM node:12.18.3-alpine3.11 as build-stage
WORKDIR /app

COPY package*.json ./
RUN yarn install

COPY . .
RUN yarn build

ENV HOST 0.0.0.0
EXPOSE 3000
CMD [ "yarn", "start" ]
